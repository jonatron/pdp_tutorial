// Javascript PDP 11/70 Emulator v1.3
// written by Paul Nankervis
// Please send suggestions, fixes and feedback to paulnank@hotmail.com
// I'm particularly interested in hearing from anyone with real experience on a PDP 11/70 front panel
//
// This code may be used freely provided the original author name is acknowledged in any modified source code
//
// http://skn.noip.me/pdp11/pdp11.html
//
//
//		
var tty = {
    rbuf: [],
    rcsr: 0,
    xcsr: 0200,
    delCode: 127,
    del: 0
};

var display = {
    data: 0,
    address: 0,
    misc: 0x14,
    switches: 0
};

var kw11 = {
    init: 0,
    csr: 0
};


var M9312 = [
    0010037, 0000700, 0010137, 0000702, 0010437, 0000704, 0005037, 0000706,
    0052737, 0100000, 0177776, 0032737, 0040000, 0177776, 0001402, 0005237,
    0000706, 0005037, 0177776, 0000401, 0000000, 0005006, 0100404, 0102403,
    0101002, 0002401, 0101401, 0000000, 0005306, 0100003, 0001402, 0002001,
    0003401, 0000000, 0006006, 0102002, 0103001, 0001001, 0000000, 0012706,
    0125252, 0010600, 0010001, 0010102, 0010203, 0010304, 0010405, 0160501,
    0002401, 0001401, 0000000, 0006102, 0103001, 0002401, 0000000, 0060203,
    0005203, 0005103, 0060301, 0103401, 0003401, 0000000, 0006004, 0050403,
    0060503, 0005203, 0103402, 0005301, 0002401, 0000000, 0005100, 0101401,
    0000000, 0040001, 0060101, 0003001, 0003401, 0000000, 0000301, 0020127,
    0052125, 0001004, 0030405, 0003002, 0005105, 0001001, 0000000, 0112700,
    0177401, 0100001, 0000000, 0077002, 0005001, 0005201, 0077002, 0005700,
    0001002, 0005701, 0001401, 0000000, 0012706, 0000776, 0004767, 0000002,
    0000000, 0022716, 0000320, 0001401, 0000000, 0012716, 0000342, 0000207,
    0000000, 0005046, 0012746, 0000354, 0000002, 0000000, 0000137, 0000362,
    0000200, 0012705, 0160000, 0005037, 0000006, 0012737, 0000400, 0000004,
    0012706, 0000776, 0005745, 0012737, 0000714, 0000114, 0005037, 0000116,
    0012703, 0177746, 0012713, 0000014, 0012702, 0001000, 0010200, 0010010,
    0005720, 0020005, 0101774, 0010200, 0011001, 0020001, 0001401, 0000000,
    0005120, 0020005, 0101771, 0014001, 0005101, 0020001, 0001401, 0000000,
    0020002, 0001371, 0005016, 0012704, 0125252, 0012713, 0000030, 0012700,
    0004000, 0012702, 0001000, 0005104, 0010410, 0005110, 0005110, 0021004,
    0001401, 0000000, 0006037, 0177752, 0103402, 0000000, 0000461, 0105116,
    0001362, 0000402, 0000167, 0177210, 0005720, 0077223, 0012713, 0000044,
    0012700, 0006000, 0105166, 0000001, 0001344, 0012702, 0001000, 0010200,
    0010010, 0005720, 0020005, 0101774, 0012701, 0000003, 0005016, 0005737,
    0000706, 0001020, 0012716, 0000030, 0010200, 0005110, 0005110, 0020010,
    0001401, 0000000, 0005720, 0006037, 0177752, 0103402, 0000000, 0000410,
    0020005, 0101763, 0011613, 0005016, 0077121, 0000404, 0000000, 0000402,
    0012713, 0000014, 0013700, 0000700, 0013701, 0000702, 0013704, 0000704,
    0000164, 0000002, 0013704, 0177570, 0042704, 0177000, 0052704, 0173000,
    0113700, 0177571, 0006200, 0000241, 0000114, 0000000, 0041060, 0025055
];


function mapUnibus(unibusAddress) {
    var idx = (unibusAddress >> 13) & 0x1f;
    if (idx < 31) {
        if (CPU.MMR3 & 0x20) {
            unibusAddress = (CPU.unibusMap[idx] + (unibusAddress & 0x1ffe)) & 0x3ffffe;
            if (unibusAddress >= IOBASE_UNIBUS && unibusAddress < IOBASE_22BIT) panic(898);
        }
    } else {
        unibusAddress |= IOBASE_22BIT;
    }
    return unibusAddress;
}


function getData(xhr, callback, meta, block, address, count) {
    var arrayBuffer, byteArray, blockno, word, base;
    arrayBuffer = xhr.response;
    if ((xhr.status != 0 && xhr.status != 206) || !arrayBuffer) {
        meta.postProcess(1, meta, block, address, count); // NXD - read error?
    } else {
        byteArray = new Uint8Array(arrayBuffer);
        blockno = block;
        for (base = 0; base < byteArray.length;) {
            if (typeof meta.cache[blockno] === "undefined") {
                meta.cache[blockno] = [];
                for (word = 0; word < meta.blocksize; word++) {
                    if (base < byteArray.length) {
                        meta.cache[blockno][word] = (byteArray[base] & 0xff) | ((byteArray[base + 1] << 8) & 0xff00);
                    } else {
                        meta.cache[blockno][word] = 0;
                    }
                    base += 2;
                }
            } else {
                base += meta.blocksize << 1;
            }
            blockno++;
        }
        callback(meta, block, address, count);
    }
}


function getCache(callback, meta, block, address, count) {
    var sectors = ~~((count + meta.blocksize - 1) / meta.blocksize);
    var xhr = new XMLHttpRequest();
    if (sectors < 2048 && block + 2048 < meta.maxblock) sectors = 2048; // make it a decent read
    xhr.open("GET", meta.url, true);
    xhr.setRequestHeader("Range", "bytes=" + ((block * meta.blocksize) << 1) + "-" +
        ((((block + sectors) * meta.blocksize) << 1) - 1));
    xhr.responseType = "arraybuffer";
    xhr.onreadystatechange = function() {
        if (xhr.readyState == xhr.DONE) {
            getData(xhr, callback, meta, block, address, count);
        }
    };
    xhr.send(null);
}


function readData(meta, block, address, count) {
    var word;
    while (count > 0) {
        if (typeof meta.cache[block] === "undefined") {
            getCache(readData, meta, block, address, count);
            return;
        }
        for (word = 0; word < meta.blocksize && count > 0; word++) {
            if (writeWordByAddr((meta.mapped ? mapUnibus(address) : address), meta.cache[block][word]) < 0) {
                meta.postProcess(2, meta, block, address, count); // NXM
                return;
            }
            //if (meta.increment) {
            address += 2;
            //}
            --count;
        }
        block++;
    }
    meta.postProcess(0, meta, block, address, count); // success
}


function writeData(meta, block, address, count) {
    var data;
    while (count > 0) {
        if (typeof meta.cache[block] === "undefined") {
            meta.cache[block] = [];
            for (word = 0; word < meta.blocksize; word++) {
                meta.cache[block][word] = 0;
            }
        }
        for (word = 0; word < meta.blocksize && count > 0; word++) {
            data = readWordByAddr((meta.mapped ? mapUnibus(address) : address));
            if (data < 0) {
                meta.postProcess(2, meta, block, address, count); // NXM
                return;
            }
            meta.cache[block][word] = data;
            //if (meta.increment) {
            address += 2;
            //}
            --count;
        }
        block++;
    }
    meta.postProcess(0, meta, block, address, count); // success
}


var rk11 = {
    rkds: 04700, // 017777400 Drive Status
    rker: 0, // 017777402 Error Register
    rkcs: 0200, // 017777404 Control Status
    rkwc: 0, // 017777406 Word Count
    rkba: 0, // 017777410 Bus Address
    rkda: 0, // 017777412 Disk Address
    rkdb: 0, // 017777416 Data Buffer
    meta: [],
    TRACKS: [406, 406, 406, 406],
    SECTORS: [12, 12, 12, 12]
};


function rk11_go() {
    var sector, address, count;
    var drive = (rk11.rkda >> 13) & 7;
    if (typeof rk11.meta[drive] === "undefined") {
        rk11.meta[drive] = {
            "cache": [],
            "postProcess": rkend,
            "drive": drive,
            "blocksize": 256,
            "mapped": 0,
            "maxblock": rk11.TRACKS[drive] * rk11.SECTORS[drive],
            "url": "rk" + drive + ".dsk"
        };
    }
    rk11.rkcs &= ~0x80; // turn off done bit
    switch ((rk11.rkcs >> 1) & 7) { // function code
        case 0: // controller reset
            rk11.rkds = 04700; // Set bits 6, 7, 8, 11
            rk11.rker = 0; //
            rk11.rkcs = 0200;
            rk11.rkda = 0;
            break;
        case 1: // write
            if (((rk11.rkda >> 4) & 0x1ff) >= rk11.TRACKS[drive]) {
                rk11.rker |= 0x8040; // NXC
                rk11.rkcs |= 0xc000; // err
                break;
            }
            if ((rk11.rkda & 0xf) >= rk11.SECTORS[drive]) {
                rk11.rker |= 0x8020; // NXS
                rk11.rkcs |= 0xc000; // err
                break;
            }
            sector = (((rk11.rkda >> 4) & 0x1ff) * rk11.SECTORS[drive] + (rk11.rkda & 0xf));
            address = (((rk11.rkcs & 0x30)) << 12) | rk11.rkba;
            count = (0x10000 - rk11.rkwc) & 0xffff;
            writeData(rk11.meta[drive], sector, address, count);
            return;
            break;
        case 2: // read
            if (((rk11.rkda >> 4) & 0x1ff) >= rk11.TRACKS[drive]) {
                rk11.rker |= 0x8040; // NXC
                rk11.rkcs |= 0xc000; // err
                break;
            }
            if ((rk11.rkda & 0xf) >= rk11.SECTORS[drive]) {
                rk11.rker |= 0x8020; // NXS
                rk11.rkcs |= 0xc000; // err
                break;
            }
            sector = (((rk11.rkda >> 4) & 0x1ff) * rk11.SECTORS[drive] + (rk11.rkda & 0xf));
            address = (((rk11.rkcs & 0x30)) << 12) | rk11.rkba;
            count = (0x10000 - rk11.rkwc) & 0xffff;
            readData(rk11.meta[drive], sector, address, count);
            return;
            break;
        case 3: // Write Check
            break;
        case 4: // Seek - complete immediately
            break;
        default:
            break;
    }
    rk11.rkds = (drive << 13) | (rk11.rkds & 0x1ff0) | ((rk11.rkda % 9) & 0xf)
    interrupt(20, 5 << 5, 0220, function() {
        rk11.rkcs = (rk11.rkcs & 0xfffe) | 0x80; // turn off go & set done
        if (rk11.rkcs & 0x40) return true;
        return false;
    });
}


function rkend(err, meta, block, address, count) {
    rk11.rkba = address & 0xffff;
    rk11.rkcs = (rk11.rkcs & ~0x30) | ((address >> 12) & 0x30);
    rk11.rkwc = (0x10000 - count) & 0xffff;
    switch (err) {
        case 1: // read error
            rk11.rker |= 0x8100; // Report TE (Timing error)
            rk11.rkcs |= 0xc000; // err
            break;
        case 2: // NXM
            rk11.rker |= 0x8400; // NXM
            rk11.rkcs |= 0xc000; // err
            break;
    }
    interrupt(20, 5 << 5, 0220, function() {
        rk11.rkcs = (rk11.rkcs & 0xfffe) | 0x80; // turn off go & set done
        if (rk11.rkcs & 0x40) return true;
        return false;
    });
}


var rl11 = {
    csr: 0x81, // 017774400 Control status register
    bar: 0, // 017774402 Bus address
    dar: 0, // 017774404 Disk address
    mpr: 0, // 017774406 Multi purpose
    bae: 0, // 017774410 Bus address extension
    DAR: 0, // internal disk address
    meta: [], // sector cache
    SECTORS: [40, 40, 40, 40], // sectors per track
    TRACKS: [1024, 1024, 512, 512], // First two drives RL02 - last two RL01 - cylinders * 2
    STATUS: [0235, 0235, 035, 035] // First two drives RL02 - last two RL01
};


function rl11_go() {
    var sector, address, count;
    var drive = (rl11.csr >> 8) & 3;
    rl11.csr &= ~0x1; // ready bit (0!)
    if (typeof rl11.meta[drive] === "undefined") {
        rl11.meta[drive] = {
            "cache": [],
            "postProcess": rlend,
            "drive": drive,
            "blocksize": 128,
            "mapped": 1,
            "maxblock": rl11.TRACKS[drive] * rl11.SECTORS[drive],
            "url": "rl" + drive + ".dsk"
        };
    }
    switch ((rl11.csr >> 1) & 7) { // function code
        case 0: // no op
            break;
        case 1: // write check
            break;
        case 2: // get status
            if (rl11.mpr & 8) rl11.csr &= 0x3f;
            rl11.mpr = rl11.STATUS[drive] | (rl11.DAR & 0100); // bit 6 Head Select bit 7 Drive Type 1=rl02
            break;
        case 3: // seek
            if ((rl11.dar & 3) == 1) {
                if (rl11.dar & 4) {
                    rl11.DAR = ((rl11.DAR + (rl11.dar & 0xff80)) & 0xff80) | ((rl11.dar << 2) & 0x40);
                } else {
                    rl11.DAR = ((rl11.DAR - (rl11.dar & 0xff80)) & 0xff80) | ((rl11.dar << 2) & 0x40);
                }
                rl11.dar = rl11.DAR;
            }
            break;
        case 4: // read header
            rl11.mpr = rl11.DAR;
            break;
        case 5: // write
            if ((rl11.dar >> 6) >= rl11.TRACKS[drive]) {
                rl11.csr |= 0x9400; // HNF
                break;
            }
            if ((rl11.dar & 0x3f) >= rl11.SECTORS[drive]) {
                rl11.csr |= 0x9400; // HNF
                break;
            }
            sector = ((rl11.dar >> 6) * rl11.SECTORS[drive]) + (rl11.dar & 0x3f);
            address = (((rl11.bae & 0x3f)) << 16) | rl11.bar; // 22 bit mode
            count = (0x10000 - rl11.mpr) & 0xffff;
            writeData(rl11.meta[drive], sector, address, count);
            return;
            break;
        case 6: // read
            if ((rl11.dar >> 6) >= rl11.TRACKS[drive]) {
                rl11.csr |= 0x9400; // HNF
                break;
            }
            if ((rl11.dar & 0x3f) >= rl11.SECTORS[drive]) {
                rl11.csr |= 0x9400; // HNF
                break;
            }
            sector = ((rl11.dar >> 6) * rl11.SECTORS[drive]) + (rl11.dar & 0x3f);
            address = (((rl11.bae & 0x3f)) << 16) | rl11.bar; // 22 bit mode
            count = (0x10000 - rl11.mpr) & 0xffff;
            readData(rl11.meta[drive], sector, address, count);
            return;
            break;
        case 7: // Read data without header check
            break;
    }
    interrupt(20, 5 << 5, 0160, function() {
        rl11.csr |= 0x81; // turn off go & set ready
        if (rl11.csr & 0x40) return true;
        return false;
    });
}


function rlend(err, meta, block, address, count) {
    var sector = block;
    rl11.bar = address & 0xffff;
    rl11.csr = (rl11.csr & ~0x30) | ((address >> 12) & 0x30);
    rl11.bae = (address >> 16) & 0x3f; // 22 bit mode
    rl11.dar = ((~~(sector / rl11.SECTORS[meta.drive])) << 6) | (sector % rl11.SECTORS[meta.drive]);
    rl11.DAR = rl11.dar;
    rl11.mpr = (0x10000 - count) & 0xffff;
    switch (err) {
        case 1: // read error
            rl11.csr |= 0x8400; // Report operation incomplete
            break;
        case 2: // NXM
            rl11.csr |= 0xa000; // NXM
            break;
    }
    interrupt(20, 5 << 5, 0160, function() {
        rl11.csr |= 0x81; // turn off go & set ready
        if (rl11.csr & 0x40) return true;
        return false;
    });
}


var rp11 = {
    DTYPE: [020022, 020020, 020022, 020042], // Drive type rp04, rp06, rp07
    SECTORS: [22, 22, 22, 50], // sectors per track 
    SURFACES: [19, 19, 19, 32], //
    CYLINDERS: [815, 411, 815, 630],
    meta: [], //meta data for drive
    rpcs1: 0x880, // Massbus 00 - actual register is a mix of controller and drive bits :-(
    rpwc: 0,
    rpba: 0,
    rpda: [0, 0, 0, 0, 0, 0, 0, 0], // Massbus 05
    rpcs2: 0,
    rpds: [0x11c0, 0x11c0, 0x11c0, 0x11c0, 0x11c0, 0x11c0, 0x11c0, 0x11c0], // Massbus 01 Read only
    rper1: [0, 0, 0, 0, 0, 0, 0, 0], // Massbus 02
    rpas: 0, // Massbus 04???
    rpla: [0, 0, 0, 0, 0, 0, 0, 0], // Massbus 07 Read only
    rpdb: 0,
    rpmr: [0, 0, 0, 0, 0, 0, 0, 0], // Massbus 03
    rpdt: [0, 0, 0, 0, 0, 0, 0, 0], // Massbus 06 Read only
    rpsn: [1, 2, 3, 4, 5, 6, 7, 8], // Massbus 10 Read only
    rpof: [0, 0, 0, 0, 0, 0, 0, 0], // Massbus 11
    rpdc: [0, 0, 0, 0, 0, 0, 0, 0], // Massbus 12
    rpcc: [0, 0, 0, 0, 0, 0, 0, 0], // Massbus 13 Read only
    rper2: [0, 0, 0, 0, 0, 0, 0, 0], // Massbus 14
    rper3: [0, 0, 0, 0, 0, 0, 0, 0], // Massbus 15
    rpec1: [0, 0, 0, 0, 0, 0, 0, 0], // Massbus 16 Read only
    rpec2: [0, 0, 0, 0, 0, 0, 0, 0], // Massbus 17 Read only
    rpbae: 0,
    rpcs3: 0
};


function rp11_init() {
    rp11.rpcs1 = 0x880;
    rp11.rpcs2 = 0;
    rp11.rpds = [0x11c0, 0x11c0, 0x11c0, 0x11c0, 0x11c0, 0x11c0, 0x11c0, 0x11c0];
    rp11.rper1 = [0, 0, 0, 0, 0, 0, 0, 0];
    rp11.rpas = rp11.rpwc = rp11.rpba = rp11.rpbae = rp11.rpcs3 = 0;
}


function rp11_attention(drive, flags) {
    rp11.rpas |= 1 << drive;
    rp11.rpds[drive] |= 0x8080;
    if (flags) {
        rp11.rper1[drive] |= flags;
        rp11.rpds[drive] |= 0x4000;
    }
}


//optional increment :-(
// cs1 is half in drive and half in controller :-(

function rp11_go(drive) {
    var sector, address, count;
    rp11.rpcs1 &= ~0x4080; // Turn TRE & ready off
    rp11.rpcs2 &= ~0x800; // Turn off NEM (NXM)
    rp11.rpds[drive] &= ~0x480; // Turn off LST & ATA
    interrupt(-1, 0, 0254); //remove pending interrupt
    if (typeof rp11.meta[drive] === "undefined") {
        rp11.meta[drive] = {
            "cache": [],
            "postProcess": rpend,
            "drive": drive,
            "blocksize": 256,
            "mapped": 0,
            "maxblock": rp11.CYLINDERS[0] * rp11.SURFACES[0] * rp11.SECTORS[0],
            "url": "rp" + drive + ".dsk"
        };
    }
    switch (rp11.rpcs1 & 0x3f) { // function code
        case 01: // NULL
            break;
        case 05: // seek
            rp11.rpcc[drive] = rp11.rpdc[drive];
            rp11.rpcs1 |= 0x8080;
            rp11.rpds[drive] |= 0x8080; // ATA
            rp11.rpas |= 1 << drive;
            break;
        case 011: // init
            rp11_init();
            break;
        case 023: // pack ack
            rp11.rpds[drive] |= 0x40; // set VV
            break;
        case 061: // write
            if (rp11.rpdc[drive] >= rp11.CYLINDERS[0] || (rp11.rpda[drive] >> 8) >= rp11.SURFACES[0] ||
                (rp11.rpda[drive] & 0xff) >= rp11.SECTORS[0]) {
                rp11.rper1[drive] |= 0x400; // invalid sector address
                rp11.rpcs1 |= 0xc000; // set SC & TRE
                break;
            }
            sector = (rp11.rpdc[drive] * rp11.SURFACES[0] + (rp11.rpda[drive] >> 8)) * rp11.SECTORS[0] + (rp11.rpda[drive] & 0xff);
            address = ((rp11.rpbae & 0x3f) << 16) | rp11.rpba; // 22 bit mode
            count = (0x10000 - rp11.rpwc) & 0xffff;
            writeData(rp11.meta[drive], sector, address, count);
            return;
            break;
        case 071: // read
            if (rp11.rpdc[drive] >= rp11.CYLINDERS[0] || (rp11.rpda[drive] >> 8) >= rp11.SURFACES[0] ||
                (rp11.rpda[drive] & 0xff) >= rp11.SECTORS[0]) {
                rp11.rper1[drive] |= 0x400; // invalid sector address
                rp11.rpcs1 |= 0xc000; // set SC & TRE
                break;
            }
            sector = (rp11.rpdc[drive] * rp11.SURFACES[0] + (rp11.rpda[drive] >> 8)) * rp11.SECTORS[0] + (rp11.rpda[drive] & 0xff);
            address = ((rp11.rpbae & 0x3f) << 16) | rp11.rpba; // 22 bit mode
            count = (0x10000 - rp11.rpwc) & 0xffff;
            readData(rp11.meta[drive], sector, address, count);
            return;
            break;
        default:
            break;
    }
    interrupt(3, 5 << 5, 0254, function() {
        rp11.rpcs1 = (rp11.rpcs1 & 0xfffe) | 0x80; // Turn go off and ready on
        rp11.rpds[drive] |= 0x80; // ATA
        if (rp11.rpcs1 & 0x40) return true;
        return false;
    });
}


function rpend(err, meta, block, address, count) {
    var surface, sector;
    rp11.rpwc = (0x10000 - count) & 0xffff;
    rp11.rpba = address & 0xffff;
    rp11.rpcs1 = (rp11.rpcs1 & ~0x300) | ((address >> 8) & 0x300);
    rp11.rpbae = (address >> 16) & 0x3f; // 22 bit mode
    sector = ~~(block / rp11.SECTORS[0]);
    surface = ~~(sector / rp11.SURFACES[0]);
    rp11.rpda[meta.drive] = ((sector % rp11.SURFACES[0]) << 8) | (block % rp11.SECTORS[0]);
    rp11.rpcc[meta.drive] = rp11.rpdc[meta.drive] = surface;
    rp11.rpas |= 1 << meta.drive;
    if (block >= meta.maxblock - 1) {
        rp11.rpds[meta.drive] |= 0x400; // LST
    }
    switch (err) {
        case 1: // read error
            rp11.rpcs2 |= 0x200; // MXF Missed transfer
            rp11.rpcs1 |= 0xc000; // set SC & TRE
            break;
        case 2: // NXM
            rp11.rpcs2 |= 0x800; // NEM (NXM)
            rp11.rpcs1 |= 0xc000; // set SC & TRE
            break;
    }
    interrupt(20, 5 << 5, 0254, function() {
        rp11.rpds[meta.drive] |= 0x80;
        rp11.rpcs1 = (rp11.rpcs1 & 0xfffe) | 0x80; // Turn go off and ready on
        if (rp11.rpcs1 & 0x40) return true;
        return false;
    });
}


function kw11_interrupt() {
    kw11.csr |= 0x80;
    if (kw11.csr & 0x40) {
        interrupt(0, 6 << 5, 0100);
    }
}


function reset_iopage() {
    CPU.stackLimit = 0xff;
    CPU.CPU_Error = 0;
    CPU.interruptQueue = [];
    CPU.pir = 0;
    CPU.MMR0 = CPU.MMR1 = CPU.MMR2 = CPU.MMR3 = CPU.mmuFrozen = CPU.mmuEnable = 0;
    CPU.mmuMask[0] = CPU.mmuMask[1] = CPU.mmuMask[3] = 0x7;
    CPU.mmuLastMode = 0;
    display.misc = (display.misc & ~0x77) | 0x14; // kernel 16 bit
    tty.rcsr = 0;
    tty.xcsr = 0200;
    kw11.csr = 0;
    rk11.rkcs = 0200;
    rl11.csr = 0x80;
    rp11_init();

}


function insertData(original, physicalAddress, data, byteFlag) {
    if (physicalAddress & 1) {
        if (!byteFlag) {
            //log.push("TRAP 4 201 " + physicalAddress.toString(8) + " " + data.toString(8));
            return trap(4, 122);
        }
        if (data >= 0) {
            data = ((data << 8) & 0xff00) | (original & 0xff);
        } else {
            data = original;
        }
    } else {
        if (data >= 0) {
            if (byteFlag) {
                data = (original & 0xff00) | (data & 0xff);
            }
        } else {
            data = original;
        }
    }
    return data;
}


function access_iopage(physicalAddress, data, byteFlag) {
    var result, idx;
    switch (physicalAddress & ~077) {
        case 017777700: // 017777700 - 017777777
            switch (physicalAddress & ~1) {
                case 017777776: // PSW
                    result = readPSW();
                    if (data >= 0) {
                        if (physicalAddress & 1) {
                            data = (data << 8) | (result & 0xff);
                        } else {
                            if (byteFlag) data = (result & 0xff00) | (data & 0xff);
                        }
                        data = (data & 0xf8ef) | (result & 0x0710);
                        writePSW(data);
                        return -1; // KLUDGE - no trap but abort any CC updates
                    }
                    break;
                case 017777774: // stack limit
                    if (data < 0) {
                        result = CPU.stackLimit & 0xff00;
                    } else {
                        if (physicalAddress & 1) {
                            data = data << 8;
                        }
                        CPU.stackLimit = data | 0xff;
                        result = 0;
                    }
                    //log.push("stacklimit access "+CPU.stackLimit.toString(8));
                    break;
                case 017777772: // PIR
                    if (data < 0) {
                        result = CPU.pir;
                    } else {
                        if (physicalAddress & 1) {
                            data = data << 8;
                        }
                        result = data & 0xfe00;
                        if (result) {
                            idx = result >> 9;
                            do {
                                result += 0x22;
                            } while (idx >>= 1);
                        }
                        CPU.pir = result;
                        CPU.priorityReview = 2;
                    }
                    break;
                case 017777766: // CPU error
                    if (CPU.cpuType !== 70) return trap(4, 222);
                    if (data < 0) {
                        result = CPU.CPU_Error;
                    } else {
                        result = CPU.CPU_Error = 0; // Always writes as zero?
                    }
                    break;
                case 017777764: // System I/D
                    if (CPU.cpuType !== 70) return trap(4, 224);
                    result = 1;
                    break;
                case 017777762: // Upper size
                    if (CPU.cpuType !== 70) return trap(4, 226);
                    result = 0;
                    break;
                case 017777760: // Lower size
                    if (CPU.cpuType !== 70) return trap(4, 228);
                    result = (MAX_MEMORY >> 6) - 1;
                    break;
                case 017777770: // Microprogram break
                    if (data >= 0 && !(physicalAddress & 1)) data &= 0xff; // Required for KB11-CM without MFPT instruction
                case 017777756: //
                case 017777754: //
                case 017777752: // Hit/miss
                case 017777750: // Maintenance
                case 017777746: // Cache control
                case 017777744: // Memory system error
                case 017777742: // High error address
                case 017777740: // Low error address
                    if (CPU.cpuType !== 70) return trap(4, 232);
                    idx = (physicalAddress - 017777740) >> 1;
                    if (data < 0) {
                        result = CPU.controlReg[idx];
                    } else {
                        result = insertData(CPU.controlReg[idx], physicalAddress, data, byteFlag);
                        if (result >= 0) {
                            CPU.controlReg[idx] = result;
                        }
                    }
                    break;
                case 017777716: // User and Super SP
                    if (physicalAddress & 1) {
                        if ((CPU.PSW >> 14) & 3 == 3) { // User Mode
                            if (data >= 0) CPU.registerVal[6] = data;
                            result = CPU.registerVal[6];
                        } else {
                            if (data >= 0) CPU.stackPointer[3] = data;
                            result = CPU.stackPointer[3];
                        }
                    } else {
                        if ((CPU.PSW >> 14) & 3 == 1) { // Super Mode
                            if (data >= 0) CPU.registerVal[6] = data;
                            result = CPU.registerVal[6];
                        } else {
                            if (data >= 0) CPU.stackPointer[1] = data;
                            result = CPU.stackPointer[1];
                        }
                    }
                    return result;
                case 017777714:
                case 017777712:
                case 017777710: // Register set 1
                    idx = physicalAddress & 7;
                    if (CPU.PSW & 0x800) {
                        if (data >= 0) CPU.registerVal[idx] = data;
                        result = CPU.registerVal[idx];
                    } else {
                        if (data >= 0) CPU.registerAlt[idx] = data;
                        result = CPU.registerAlt[idx];
                    }
                    return result;
                case 017777706: // Kernel SP & PC
                    if (physicalAddress & 1) {
                        if (data >= 0) CPU.registerVal[7] = data;
                        result = CPU.registerVal[7];
                    } else {
                        if ((CPU.PSW >> 14) & 3 == 0) { // Kernel Mode
                            if (data >= 0) CPU.registerVal[6] = data;
                            result = CPU.registerVal[6];
                        } else {
                            if (data >= 0) CPU.stackPointer[0] = data;
                            result = CPU.stackPointer[0];
                        }
                    }
                    return result;
                case 017777704:
                case 017777702:
                case 017777700: // Register set 0
                    idx = physicalAddress & 7;
                    if (CPU.PSW & 0x800) {
                        if (data >= 0) CPU.registerAlt[idx] = data;
                        result = CPU.registerAlt[idx];
                    } else {
                        if (data >= 0) CPU.registerVal[idx] = data;
                        result = CPU.registerVal[idx];
                    }
                    return result;
                default:
                    CPU.CPU_Error |= 0x10;
                    return trap(4, 124);
            }
            break;
        case 017777600: // 017777600 - 017777677 MMU user mode Map
            idx = (physicalAddress >> 1) & 037;
            result = insertData(CPU.mmuMap[3][idx], physicalAddress, data, byteFlag);
            if (result >= 0) {
                CPU.mmuMap[3][idx] = result;
                CPU.mmuMap[3][idx & 0xf] &= 0xff0f;
            }
            break;
        case 017777500: // 017777500 - 017777577
            switch (physicalAddress & ~1) {
                case 017777576: // MMR2
                    result = insertData(CPU.MMR2, physicalAddress, data, byteFlag);
                    if (result >= 0) CPU.MMR2 = result;
                    break;
                case 017777574: // MMR1
                    result = CPU.MMR1;
                    if (result & 0xff00) result = ((result << 8) | (result >> 8)) & 0xffff;
                    break;
                case 017777572: // MMR0
                    CPU.MMR0 = (CPU.MMR0 & 0xf381) | (CPU.mmuLastMode << 5) | (CPU.mmuLastPage << 1);
                    if (data < 0) {
                        result = CPU.MMR0;
                    } else {
                        result = insertData(CPU.MMR0, physicalAddress, data, byteFlag);
                        if (result >= 0) {
                            CPU.MMR0 = result &= 0xf381;
                            CPU.mmuLastMode = (result >> 5) & 3;
                            CPU.mmuLastPage = (result >> 1) & 0xf;
                            if (result & 0x101) {
                                idx = 2; // 18 bit
                                if (CPU.MMR3 & 0x10) idx = 1; // 22 bit
                                if (result & 0x1) {
                                    CPU.mmuEnable = READ_MODE | WRITE_MODE;
                                } else {
                                    CPU.mmuEnable = WRITE_MODE;
                                }
                            } else {
                                CPU.mmuEnable = 0;
                                idx = 4; // 16 bit
                            }
                            display.misc = (display.misc & ~7) | idx;
                        }
                    }
                    break;
                case 017777570: // console display/switch;
                    if (data < 0) {
                        result = display.switches & 0xffff;
                    } else {
                        result = insertData(display.data, physicalAddress, data, byteFlag);
                        if (result >= 0) display.data = result;
                    }
                    break;
                case 017777566: // console xbuf
                    if (data >= 0) {
                        data &= 0x7f;
                        if (data) {
                        switch (data) {
                            case 0:
                            case 015:
                                if (document.forms.console.line.value.length > 9000) {
                                    document.forms.console.line.value = document.forms.console.line.value.substring(document.forms.console.line.value.length - 8192);
                                }
                                break;
                            case 010:
                                if (!tty.del) {
                                    document.forms.console.line.value = document.forms.console.line.value.substring(0, document.forms.console.line.value.length - 1);
                                }
                                break;
                            default:
                                document.forms.console.line.value += String.fromCharCode(data);
                                if (data == 012) {
                                    document.forms.console.line.scrollTop = document.forms.console.line.scrollHeight;
                                }
                        }
                        tty.del = 0;
                        }
                        tty.xcsr &= ~0200;
                        interrupt(100, 4 << 5, 064, function() {
                            tty.xcsr |= 0200;
                            if (tty.xcsr & 0100) return true;
                            return false;
                        });
                    }
                    result = 0;
                    break;
                case 017777564: // console xcsr
                    if (data < 0) {
                        result = tty.xcsr;
                    } else {
                        result = insertData(tty.xcsr, physicalAddress, data, byteFlag);
                        if (result >= 0) {
                            if ((tty.xcsr & 0300) == 0200 && (result & 0100)) {
                                interrupt(8, 4 << 5, 064, function() {
                                    tty.xcsr |= 0200;
                                    if (tty.xcsr & 0100) return true;
                                    return false;
                                });
                            }
                            tty.xcsr = (tty.xcsr & 0200) | (result & ~0200);
                        }
                    }
                    break;
                case 017777562: // console rbuf
                    result = 0;
                    if (data < 0) {
                        tty.rcsr &= ~0200;
                        if (tty.rbuf.length > 0) {
                            result = tty.rbuf.shift();
                            if (tty.rbuf.length > 0) {
                                setTimeout(function() {
                                    tty.rcsr |= 0200;
                                    if (tty.rcsr & 0100) interrupt(40, 4 << 5, 060);
                                }, 50);
                            }
                        }
                    }
                    break;
                case 017777560: // console rcsr
                    if (data < 0) {
                        result = tty.rcsr;
                    } else {
                        result = insertData(tty.rcsr, physicalAddress, data, byteFlag);
                        if (result >= 0) tty.rcsr = (tty.rcsr & 0200) | (result & ~0200);
                    }
                    break;
                case 017777546: // kw11.csr
                    if (data < 0) {
                        result = kw11.csr;
                        kw11.csr &= ~0200;
                    } else {
                        result = insertData(kw11.csr, physicalAddress, data, byteFlag);
                        if (result >= 0) {
                            if (result & 0100) {
                                if (!kw11.init) {
                                    setInterval(kw11_interrupt, 25);
                                    kw11.init = 1;
                                }
                            }
                            kw11.csr = result & ~0200;
                        }
                    }
                    break;
                default:
                    CPU.CPU_Error |= 0x10;
                    return trap(4, 126);
            }
            break;
        case 017777400: // 017777400 - 017777477
            switch (physicalAddress & ~1) {
                case 017777400: // rk11.rkds
                    result = insertData(rk11.rkds, physicalAddress, data, byteFlag);
                    if (result >= 0) rk11.rkds = result;
                    break;
                case 017777402: // rk11.rker
                    result = insertData(rk11.rker, physicalAddress, data, byteFlag);
                    if (result >= 0) rk11.rker = result;
                    break;
                case 017777404: // rk11.rkcs
                    result = insertData(rk11.rkcs, physicalAddress, data, byteFlag);
                    if (data >= 0 && result >= 0) {
                        rk11.rkcs = (result & ~0x9080) | (rk11.rkcs & 0x9080); // Bits 7 and 12 - 15 are read only
                        if (rk11.rkcs & 1) {
                            rk11_go();
                        }
                    }
                    break;
                case 017777406: // rk11.rkwc
                    result = insertData(rk11.rkwc, physicalAddress, data, byteFlag);
                    if (result >= 0) rk11.rkwc = result;
                    break;
                case 017777410: // rk11.rkba
                    result = insertData(rk11.rkba, physicalAddress, data, byteFlag);
                    if (result >= 0) rk11.rkba = result;
                    break;
                case 017777412: // rk11.rkda
                    result = insertData(rk11.rkda, physicalAddress, data, byteFlag);
                    if (result >= 0) rk11.rkda = result;
                    break;
                case 017777414: // rk11.unused
                    break;
                case 017777416: // rk11.rkdb
                    result = insertData(rk11.rkdb, physicalAddress, data, byteFlag);
                    if (result >= 0) rk11.rkdb = result;
                    break;
                default:
                    CPU.CPU_Error |= 0x10;
                    return trap(4, 128);
            }
            break;
        case 017776700: // 017777600 - 017777677 rp11 controller
            //if (physicalAddress != 017776700) console.log("RP11 register " + physicalAddress.toString(8) + " " + data.toString(8));
            idx = rp11.rpcs2 & 7;
            switch (physicalAddress & ~1) {
                case 017776700: // rp11.rpcs1 Control status 1
                    result = insertData(rp11.rpcs1, physicalAddress, data, byteFlag);
                    if (data >= 0 && result >= 0) {
                        rp11.rpbae = (rp11.rpbae & 0x3c) | ((result >> 8) & 0x3);
                        rp11.rpcs1 = (result & 0x437f) | (rp11.rpcs1 & 0x8880) | 0x800;
                        if (result & 1 && (rp11.rpcs1 & 0x80)) {
                            rp11_go(idx);
                        } else {
                            if ((result & 0xc0) == 0xc0) {
                                interrupt(0, 5 << 5, 0254);
                            }
                        }
                    }
                    break;
                case 017776702: // rp11.rpwc  Word count
                    result = insertData(rp11.rpwc, physicalAddress, data, byteFlag);
                    if (result >= 0) rp11.rpwc = result;
                    break;
                case 017776704: // rp11.rpba  Memory address
                    result = insertData(rp11.rpba, physicalAddress, data, byteFlag);
                    if (result >= 0) rp11.rpba = result & 0xfffe; // must be even
                    break;
                case 017776706: // rp11.rpda  Disk address
                    result = insertData(rp11.rpda[idx], physicalAddress, data, byteFlag);
                    if (result >= 0) rp11.rpda[idx] = result & 0x1f1f;
                    break;
                case 017776710: // rp11.rpcs2 Control status 2
                    result = insertData(rp11.rpcs2, physicalAddress, data, byteFlag);
                    if (result >= 0) {
                        rp11.rpcs2 = (result & 0x3f) | (rp11.rpcs2 & 0xffc0);
                        if (result & 0200) rp11_init();
                    }
                    break;
                case 017776712: // rp11.rpds  drive status
                    result = rp11.rpds[idx];
                    break;
                case 017776714: // rp11.rper1 Error 1
                    result = rp11.rper1[idx];
                    break;
                case 017776716: // rp11.rpas  Attention summary
                    result = insertData(rp11.rpas, physicalAddress, data, byteFlag);
                    if (result >= 0) rp11.rpas = result & 0xff;
                    break;
                case 017776720: // rp11.rpla  Look ahead
                    result = rp11.rpla[idx];
                    break;
                case 017776722: // rp11.rpdb  Data buffer
                    result = insertData(rp11.rpdb, physicalAddress, data, byteFlag);
                    if (result >= 0) rp11.rpdb = result;
                    break;
                case 017776724: // rp11.rpmr  Maintenance
                    result = insertData(rp11.rpmr[idx], physicalAddress, data, byteFlag);
                    if (result >= 0) rp11.rpmr[idx] = result & 0x3ff;
                    break;
                case 017776726: // rp11.rpdt  drive type read only
                    result = 020022; // rp11.rpdt[idx];
                    break;
                case 017776730: // rp11.rpsn  Serial number read only
                    result = rp11.rpsn[idx];
                    break;
                case 017776732: // rp11.rpof  Offset register
                    result = insertData(rp11.rpof[idx], physicalAddress, data, byteFlag);
                    if (result >= 0) rp11.rpof[idx] = result;
                    break;
                case 017776734: // rp11.rpdc  Desired cylinder
                    result = insertData(rp11.rpdc[idx], physicalAddress, data, byteFlag);
                    if (result >= 0) rp11.rpdc[idx] = result & 0x1ff;
                    break;
                case 017776736: // rp11.rpcc  Current cylinder read only
                    rp11.rpcc[idx] = rp11.rpdc[idx];
                    result = rp11.rpcc[idx];
                    break;
                case 017776740: // rp11.rper2 Error 2
                    result = rp11.rper2[idx];
                    break;
                case 017776742: // rp11.rper3 Error 3
                    result = rp11.rper3[idx];
                    break;
                case 017776744: // rp11.rpec1 Error correction 1 read only
                    result = rp11.rpec1[idx];
                    break;
                case 017776746: // rp11.rpec2 Error correction 2 read only
                    result = rp11.rpec2[idx];
                    break;
                case 017776750: // rp11.rpbae Bus address extension
                    result = insertData(rp11.rpbae, physicalAddress, data, byteFlag);
                    if (result >= 0) {
                        rp11.rpbae = result & 0x3f;
                        rp11.rpcs1 = (rp11.rpcs1 & ~0x300) | ((result & 0x3) << 8);
                    }
                    break;
                case 017776752: // rp11.rpcs3 Control status 3
                    result = insertData(rp11.rpcs3, physicalAddress, data, byteFlag);
                    if (result >= 0) rp11.rpcs3 = result;
                    break;
                default:
                    CPU.CPU_Error |= 0x10;
                    return trap(4, 132);
            }
            break;
        case 017774400: // 017774400 - 017774477
            switch (physicalAddress & ~1) {
                case 017774400: // rl11.csr
                    result = insertData(rl11.csr, physicalAddress, data, byteFlag);
                    if (data >= 0 && result >= 0) {
                        rl11.bae = (rl11.bae & 0x3c) | ((result >> 4) & 0x3);
                        rl11.csr = (rl11.csr & ~0x3fe) | (result & 0x3fe);
                        if (!(rl11.csr & 0x80)) {
                            rl11_go();
                        }
                    }
                    break;
                case 017774402: // rl11.bar
                    result = insertData(rl11.bar, physicalAddress, data, byteFlag);
                    if (result >= 0) {
                        rl11.bar = result & 0xfffe;
                    }
                    break;
                case 017774404: // rl11.dar
                    result = insertData(rl11.dar, physicalAddress, data, byteFlag);
                    if (result >= 0) rl11.dar = result;
                    break;
                case 017774406: // rl11.mpr
                    result = insertData(rl11.mpr, physicalAddress, data, byteFlag);
                    if (result >= 0) rl11.mpr = result;
                    break;
                case 017774410: // rl11.bae
                    result = insertData(rl11.bae, physicalAddress, data, byteFlag);
                    if (result >= 0) {
                        rl11.bae = result & 0x3f;
                        rl11.csr = (rl11.csr & ~0x30) | ((rl11.bae & 0x3) << 4);
                    }
                    break;
                default:
                    CPU.CPU_Error |= 0x10;
                    return trap(4, 134);
            }
            break;
        case 017772500: // 017772500 - 017772577
            switch (physicalAddress & ~1) {
                case 017772516: // MMR3 - UB 22 x K S U
                    if (data < 0) {
                        result = CPU.MMR3;
                    } else {
                        result = insertData(CPU.MMR3, physicalAddress, data, byteFlag);
                        if (result >= 0) {
                            if (CPU.cpuType !== 70) result &= ~0x30; // don't allow 11/45 to do 22 bit or use unibus map
                            CPU.MMR3 = result;
                            CPU.mmuMask[0] = (result & 4 ? 0xf : 0x7);
                            CPU.mmuMask[1] = (result & 2 ? 0xf : 0x7);
                            CPU.mmuMask[3] = (result & 1 ? 0xf : 0x7);
                            if (CPU.mmuEnable) {
                                idx = 2; // 18 bit
                                if (CPU.MMR3 & 0x10) idx = 1; // 22 bit
                                display.misc = (display.misc & ~7) | idx;
                            }
                        }
                    }
                    break;
                default:
                    CPU.CPU_Error |= 0x10;
                    return trap(4, 136);
            }
            break;
        case 017772300: // 017772300 - 017772377 MMU kernel mode Map
            idx = (physicalAddress >> 1) & 037;
            result = insertData(CPU.mmuMap[0][idx], physicalAddress, data, byteFlag);
            if (result >= 0) {
                CPU.mmuMap[0][idx] = result;
                CPU.mmuMap[0][idx & 0xf] &= 0xff0f;
            }
            break;
        case 017772200: // 017772200 - 017772277 MMU super mode Map
            idx = (physicalAddress >> 1) & 037;
            result = insertData(CPU.mmuMap[1][idx], physicalAddress, data, byteFlag);
            if (result >= 0) {
                CPU.mmuMap[1][idx] = result;
                CPU.mmuMap[1][idx & 0xf] &= 0xff0f;
            }
            break;
        case 017770300: // 017770300 - 017770377 Unibus Map
        case 017770200: // 017770200 - 017770277 Unibus Map
            if (CPU.cpuType !== 70) return trap(4, 234);
            idx = (physicalAddress >> 2) & 0x1f;
            result = CPU.unibusMap[idx];
            if (physicalAddress & 02) result = result >> 16;
            result &= 0xffff;
            if (data >= 0) {
                result = insertData(result, physicalAddress, data, byteFlag);
                if (result >= 0) {
                    if (physicalAddress & 02) {
                        CPU.unibusMap[idx] = (result << 16) | (CPU.unibusMap[idx] & 0xffff);
                    } else {
                        CPU.unibusMap[idx] = (CPU.unibusMap[idx] & 0xffff0000) | (result & 0xfffe);
                    }
                }
            }
            break;
        case 017765000: // 017765000 - 017765777 Bootstrap diagnostic
        case 017765100:
        case 017765200:
        case 017765300:
        case 017765400:
        case 017765500:
        case 017765600:
        case 017765700:
            if (data < 0) {
                idx = (physicalAddress >> 1) & 0xff;
                result = M9312[idx];
            } else {
                CPU.CPU_Error |= 0x10;
                return trap(4, 138);
            }
            break;
        default:
            CPU.CPU_Error |= 0x10;
            return trap(4, 142);
    }
    if (byteFlag && result >= 0) { // make any required byte adjustment
        if ((physicalAddress & 1)) {
            result = result >> 8;
        } else {
            result &= 0xff;
        }
    }
    if (typeof result === "undefined") {
        panic(76);
    }
    return result;
}
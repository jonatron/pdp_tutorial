// Javascript PDP 11/70 Emulator v1.2a
// written by Paul Nankervis
// Please send suggestions, fixes and feedback to paulnank@hotmail.com
// I'm particularly interested in hearing from anyone with real experience on a PDP 11/70 front panel
//
// This code may be used freely provided the original author name is maintained in any modified source code
//
// http://skn.noip.me/pdp11/pdp11.html
// Developed using chrome - mileage with other browsers may vary
//
//
var BOOTBASE=01000;
var bootcode=[
005000,
005200,
000005,
000775
];